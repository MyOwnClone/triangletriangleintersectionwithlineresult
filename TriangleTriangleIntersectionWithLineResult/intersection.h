//
//  intersection.h
//  TriangleTriangleIntersectionWithLineResult
//
//  Created by tomas on 19.06.14.
//  Copyright (c) 2014 myownclone. All rights reserved.
//

#ifndef __TriangleTriangleIntersectionWithLineResult__intersection__
#define __TriangleTriangleIntersectionWithLineResult__intersection__

int coplanar_tri_tri(float N[3],float V0[3],float V1[3],float V2[3],
                     float U0[3],float U1[3],float U2[3]);

int tri_tri_intersect(float V0[3],float V1[3],float V2[3],
                      float U0[3],float U1[3],float U2[3]);

int tri_tri_intersect_with_isectline(float V0[3],float V1[3],float V2[3],
                                     float U0[3],float U1[3],float U2[3],int *coplanar,
                                     float isectpt1[3],float isectpt2[3]);


#endif /* defined(__TriangleTriangleIntersectionWithLineResult__intersection__) */
