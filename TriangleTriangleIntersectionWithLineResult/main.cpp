#include <iostream>
#include "intersection.h"

int main(int argc, const char * argv[])
{
    float triangle0[][3] = { { -1, 0, 0}, { 1, 0, 0 }, { 0, 1, 0 }  };
    float triangle1[][3] = { { -1, 0, 0}, { 1, 0, 0 }, { 0, 1, 0 }  };
    
    float triangle2[][3] = { { -1, 0, 2}, { 1, 0, 2 }, { 0, 1, 2 }  };
    float triangle3[][3] = { {  0, 0, 1}, { 0, 0, -1}, { 0, 1, 0 }  };
    
    int coplanar = 0;
    
    float *resultPoint0 = new float[3];
    float *resultPoint1 = new float[3];
    
    int result = tri_tri_intersect_with_isectline(triangle0[0], triangle0[1], triangle0[2], triangle1[0], triangle1[1], triangle1[2], &coplanar, resultPoint0, resultPoint1);
    
    std::cout << "interects: " << result << std::endl;
    
    result = tri_tri_intersect_with_isectline(triangle0[0], triangle0[1], triangle0[2], triangle2[0], triangle2[1], triangle2[2], &coplanar, resultPoint0, resultPoint1);
    
    std::cout << "interects: " << result << std::endl;
    
    result = tri_tri_intersect_with_isectline(triangle0[0], triangle0[1], triangle0[2], triangle3[0], triangle3[1], triangle3[2], &coplanar, resultPoint0, resultPoint1);
    
    std::cout << "interects: " << result << std::endl;
    std::cout << "p0 " << "x: " << resultPoint0[0] << " y: " << resultPoint0[1] << " z: " << resultPoint0[2] << std::endl;
    std::cout << "p1 " << "x: " << resultPoint1[0] << " y: " << resultPoint1[1] << " z: " << resultPoint1[2] << std::endl;
    
    delete[] resultPoint0;
    delete[] resultPoint1;
}

